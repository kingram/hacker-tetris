#!/usr/bin/env python
# ******************************************************************************
# hacker_tetris.py
#
# Tetris game adapted to run on PyGame
#
# ******************************************************************************

import functions_tetris
import pygame

# MAIN *************************************************************************

# ENTIRE GAME RUNS IN WHILE LOOP
game_continue = True

# INIT SCREEN
pygame.init()
functions_tetris.screen = pygame.display.set_mode((630,450))
functions_tetris.clock = pygame.time.Clock()
done = False

while game_continue:	

	# RESET SCREEN TO BLACK
	functions_tetris.screen.fill((0,0,0))
	pygame.display.flip()

	# INTRO SCREEN
	intro_end = False
	functions_tetris.intro();
	while not intro_end:
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				intro_end = True
				game_continue = False
				exit()
			if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
				intro_end = True
	
	# RESET SCREEN TO BLACK
	functions_tetris.screen.fill((0,0,0))
	pygame.display.flip()
	
	functions_tetris.score = 0
	functions_tetris.level = 1
	
	functions_tetris.game_board()
	functions_tetris.draw_shape(functions_tetris.new_piece())
	pygame.display.flip()
	functions_tetris.play_continue = True
	
	DOWN_TIMER = pygame.USEREVENT+1
	
	while functions_tetris.play_continue:
		next_piece = functions_tetris.new_piece()
		functions_tetris.draw_next_shape(next_piece)
		functions_tetris.unlocked = True
		
		if (functions_tetris.level == 1):
			pygame.time.set_timer(DOWN_TIMER, 1000)
		elif (functions_tetris.level == 2):
			pygame.time.set_timer(DOWN_TIMER, 700)
		elif (functions_tetris.level == 3):
			pygame.time.set_timer(DOWN_TIMER, 400)
		elif (functions_tetris.level == 4):
			pygame.time.set_timer(DOWN_TIMER, 200)
		elif (functions_tetris.level == 5):
			pygame.time.set_timer(DOWN_TIMER, 100)
		
		pygame.draw.rect(functions_tetris.screen, (0, 0, 0), pygame.Rect(270, 240, 400, 30))
		
		font = pygame.font.Font(None, 24)
		text = font.render("-- LEVEL " + str(functions_tetris.level) + " --", True, (255, 255, 255))
		functions_tetris.screen.blit(text, (280 , 250))
		
		text = font.render("SCORE: " + str(functions_tetris.score), True, (255, 255, 255))
		functions_tetris.screen.blit(text, (420 , 250))
		
		while functions_tetris.unlocked:
			for event in pygame.event.get():
				if event.type == DOWN_TIMER:
					functions_tetris.down()
				if event.type == pygame.QUIT:
					game_continue = False
					functions_tetris.unlocked = False
					functions_tetris.play_continue = False
					exit()
				if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
					functions_tetris.rotate()
				if event.type == pygame.KEYDOWN and event.key == pygame.K_DOWN:
					functions_tetris.down()
				if event.type == pygame.KEYDOWN and event.key == pygame.K_RIGHT:
					functions_tetris.rshift()
				if event.type == pygame.KEYDOWN and event.key == pygame.K_LEFT:
					functions_tetris.lshift()
				
				pygame.display.flip()
				
		functions_tetris.draw_shape(next_piece)
		pygame.display.flip()
		
		
		
		# CHECK LEVEL
		if (functions_tetris.score > 100000):
			functions_tetris.level = 5
		elif (functions_tetris.score > 20000):
			functions_tetris.level = 4
		elif (functions_tetris.score > 10000):
			functions_tetris.level = 3
		elif (functions_tetris.score > 2000):
			functions_tetris.level = 2
		else:
			functions_tetris.level = 1
			
	# RESET SCREEN TO BLACK
	functions_tetris.screen.fill((0,0,0))
	pygame.display.flip()
	
	end_end = False
	functions_tetris.end_screen()
	
	while not end_end:
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				end_end = True
				game_continue = False
				exit()
			if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
				end_end = True