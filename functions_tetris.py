#!/usr/bin/env255 python
# ******************************************************************************
# functions_tetris.py
#
# Functions implemented to manage Tetris
#
# ******************************************************************************

import random
import pygame

# DEFINED VALUES (DO NOT CHANGE!!!) ********************************************

R_EDGE		= 220
L_EDGE		= 20
TOP			= 20
BOTTOM		= 420

# GLOBAL VALUES ****************************************************************

clr = (255, 255, 255)

sqx1 = 0
sqx2 = 0
sqx3 = 0
sqx4 = 0
centerx = 0

sqy1 = 0
sqy2 = 0
sqy3 = 0
sqy4 = 0
centery = 0

score = 0

unlocked = True
play_continue = True
level = 1

# SQUARE SIZE
size = 20

# INTRO ************************************************************************
# Displays game instructions and start screen

def intro():
	font = pygame.font.Font(None, 24)
	text = font.render("H a c k e r  T e t r i s", True, (255, 255, 255))
	screen.blit(text, (240 , 200))
	
	text = font.render("Press SPACEBAR to continue", True, (255, 255, 255))
	screen.blit(text, (200 , 250))
	
	pygame.display.flip()
# END SCREEN *******************************************************************
# Screen displayed when the game ends

def end_screen():
	global level

	font = pygame.font.Font(None, 24)
	text = font.render("HAHA! You LOST!", True, (255, 255, 255))
	screen.blit(text, (240 , 200))
	
	if (level == 1):
		text = font.render("You were only level 1!", True, (255, 255, 255))
		screen.blit(text, (230 , 270))
	elif (level == 2):
		text = font.render("You were only level 2!", True, (255, 255, 255))
		screen.blit(text, (230 , 270))
	elif (level == 3):
		text = font.render("You were only level 3!", True, (255, 255, 255))
		screen.blit(text, (230 , 270))
	elif (level == 4):
		text = font.render("You were only level 4!", True, (255, 255, 255))
		screen.blit(text, (230 , 270))
	elif (level == 5):
		text = font.render("You were only level 5!", True, (255, 255, 255))
		screen.blit(text, (230 , 270))
	
	text = font.render("Press SPACEBAR to continue", True, (255, 255, 255))
	screen.blit(text, (200 , 300))
	
	pygame.display.flip()

# GAME BOARD *******************************************************************
# Generates background of game board (not including pieces)

def game_board():
	pygame.draw.rect(screen, (160, 160, 160), pygame.Rect(16, 16, 10*size+8, 20*size+8))
	pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(20, 20, 10*size, 20*size))
	
	for x in range (0, 260, 20):
		for y in range(0, 460, 20):
			draw_square(x, y, (160, 160, 160))
	
	pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(16, 16, 10*size+8, 20*size+8))
	pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(20, 20, 10*size, 20*size))
	
	for x in range (260, 640, 20):
		for y in range (420, 460, 20):
			draw_square(x, y, (160, 160, 160))
	
	for x in range (260, 640, 20):
		for y in range (0, 100, 20):
			draw_square(x, y, (160, 160, 160))
	# H
	pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(264, 296, 12, 52))
	pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(304, 296, 12, 52))
	pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(264, 316, 52, 12))
	
	# A
	pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(324, 296, 12, 52))
	pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(364, 296, 12, 52))
	pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(324, 316, 52, 12))
	pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(324, 296, 52, 12))
	
	# C
	pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(384, 296, 12, 52))
	pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(384, 296, 52, 12))
	pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(384, 336, 52, 12))
	
	# K
	pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(444, 296, 12, 52))
	pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(444, 306, 52, 12))
	pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(444, 326, 52, 12))
	
	# E
	pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(504, 296, 12, 52))
	pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(504, 296, 52, 12))
	pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(504, 316, 52, 12))
	pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(504, 336, 52, 12))
	
	# R
	pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(564, 296, 12, 52))
	pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(564, 296, 52, 12))
	pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(564, 316, 52, 12))
	pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(594, 316, 12, 32))
	pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(604, 296, 12, 32))
			
	# T x=[260, 320] y=[360, 420]
	pygame.draw.rect(screen, (255, 0, 0), pygame.Rect(264, 356, 52, 12))
	pygame.draw.rect(screen, (255, 0, 0), pygame.Rect(284, 356, 12, 52))
	
	# E
	pygame.draw.rect(screen, (255, 128, 0), pygame.Rect(324, 356, 12, 52))
	pygame.draw.rect(screen, (255, 128, 0), pygame.Rect(324, 396, 52, 12))
	pygame.draw.rect(screen, (255, 128, 0), pygame.Rect(324, 356, 52, 12))
	pygame.draw.rect(screen, (255, 128, 0), pygame.Rect(324, 376, 52, 12))
	
	# T x=[260, 320] y=[360, 420]
	pygame.draw.rect(screen, (255, 255, 0), pygame.Rect(384, 356, 52, 12))
	pygame.draw.rect(screen, (255, 255, 0), pygame.Rect(404, 356, 12, 52))
	
	# R
	pygame.draw.rect(screen, (0, 255, 0), pygame.Rect(444, 356, 52, 12))
	pygame.draw.rect(screen, (0, 255, 0), pygame.Rect(444, 356, 12, 52))
	pygame.draw.rect(screen, (0, 255, 0), pygame.Rect(444, 376, 52, 12))
	pygame.draw.rect(screen, (0, 255, 0), pygame.Rect(474, 376, 12, 32))
	pygame.draw.rect(screen, (0, 255, 0), pygame.Rect(484, 356, 12, 32))
	
	# I
	pygame.draw.rect(screen, (0, 0, 255), pygame.Rect(504, 356, 52, 12))
	pygame.draw.rect(screen, (0, 0, 255), pygame.Rect(504, 396, 52, 12))
	pygame.draw.rect(screen, (0, 0, 255), pygame.Rect(524, 356, 12, 52))
	
	# S
	pygame.draw.rect(screen, (127, 0, 255), pygame.Rect(564, 356, 52, 12))
	pygame.draw.rect(screen, (127, 0, 255), pygame.Rect(564, 376, 52, 12))
	pygame.draw.rect(screen, (127, 0, 255), pygame.Rect(564, 396, 52, 12))
	pygame.draw.rect(screen, (127, 0, 255), pygame.Rect(564, 356, 12, 32))
	pygame.draw.rect(screen, (127, 0, 255), pygame.Rect(604, 376, 12, 32))
	
	pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(266, 16, 4*size+8, 2*size+8))
	pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(270, 20, 4*size, 2*size))
	pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(370, 20, 6*size, 2*size))
	
	font = pygame.font.Font(None, 24)
	text = font.render("NEXT PIECE", True, (255, 255, 255))
	screen.blit(text, (380, 32))
	
	font = pygame.font.Font(None, 24)
	text = font.render("INSTRUCTIONS:", True, (255, 255, 255))
	screen.blit(text, (280, 120))
	
	font = pygame.font.Font(None, 24)
	text = font.render("RIGHT KEY   :  RIGHT", True, (255, 255, 255))
	screen.blit(text, (320, 150))
	
	font = pygame.font.Font(None, 24)
	text = font.render("LEFT KEY      :  LEFT", True, (255, 255, 255))
	screen.blit(text, (320, 170))
	
	font = pygame.font.Font(None, 24)
	text = font.render("DOWN KEY   :  DOWN", True, (255, 255, 255))
	screen.blit(text, (320, 190))
	
	font = pygame.font.Font(None, 24)
	text = font.render("SPACE KEY  :  ROTATE", True, (255, 255, 255))
	screen.blit(text, (320, 210))

# DRAW SHAPE *******************************************************************
# Draws shape seeded by random generator

def draw_shape(p):
	global sqx1
	global sqx2
	global sqx3
	global sqx4
	global sqy1
	global sqy2
	global sqy3
	global sqy4
	global centerx
	global centery
	global clr
	global play_continue
	global score

	x = 120
	y = 40

	clr = (255, 255, 255)
	
	centerx = x
	centery = y
	
	if (p == 1):
		# SQUARE
		sqx1 = x - size/2
		sqy1 = y - size/2
		sqx2 = x + size/2
		sqy2 = y - size/2
		sqx3 = x + size/2
		sqy3 = y + size/2
		sqx4 = x - size/2
		sqy4 = y + size/2
		
		clr = (255, 255, 0)
		
	elif (p == 2):
		# LINE
		
		sqx1 = x - (size/2 + size)
		sqx2 = x - size/2
		sqx3 = x + size/2
		sqx4 = x + (size/2 + size)
		sqy1 = y - size/2
		sqy2 = y - size/2
		sqy3 = y - size/2
		sqy4 = y - size/2
		
		clr = (0, 255, 255)
	
	elif (p == 3):
		# S-SHAPE
	
		sqx1 = x + size/2
		sqx2 = x + (size/2 + size)
		sqx3 = x + size/2
		sqx4 = x - size/2
		sqy1 = y - size/2
		sqy2 = y - size/2
		sqy3 = y + size/2
		sqy4 = y + size/2
		
		clr = (0, 255, 0)
	
	elif (p == 4):
		# Z-SHAPE
		
		sqx1 = x - (size/2)
		sqx2 = x - (size/2 + size)
		sqx3 = x - (size/2)
		sqx4 = x + (size/2)
		sqy1 = y - (size/2)
		sqy2 = y - (size/2)
		sqy3 = y + (size/2)
		sqy4 = y + (size/2)
		
		clr = (255, 0, 0)
		
	elif (p == 5):
		# T-SHAPE
		
		sqx1 = x + (size/2)
		sqx2 = x + (size/2)
		sqx3 = x + (size/2 + size)
		sqx4 = x - (size/2)
		sqy1 = y - (size/2)
		sqy2 = y + (size/2)
		sqy3 = y + (size/2)
		sqy4 = y + (size/2)
		
		clr = (255, 0, 255)
	
	elif (p == 6):
		# L-SHAPE
		
		sqx1 = x + (size/2)
		sqx2 = x + (size/2)
		sqx3 = x - (size/2)
		sqx4 = x - (size/2 + size)
		sqy1 = y - (size/2)
		sqy2 = y + (size/2)
		sqy3 = y + (size/2)
		sqy4 = y + (size/2)
		
		clr = (255, 128, 0)
		
	elif (p == 7):
		# MIRRORED L-SHAPE
		
		sqx1 = x - (size/2)
		sqx2 = x - (size/2)
		sqx3 = x + (size/2)
		sqx4 = x + (size/2 + size)
		sqy1 = y - (size/2)
		sqy2 = y + (size/2)
		sqy3 = y + (size/2)
		sqy4 = y + (size/2)
		
		clr = (0, 0, 204)
		
	
	locked_color = (255, 255, 255)
	
	if (screen.get_at((int(sqx1), int(sqy1)))[:3] == locked_color):
		play_continue = False
	elif (screen.get_at((int(sqx2), int(sqy2)))[:3] == locked_color):
		play_continue = False
	elif (screen.get_at((int(sqx3), int(sqy3)))[:3] == locked_color):
		play_continue = False
	elif (screen.get_at((int(sqx4), int(sqy4)))[:3] == locked_color):
		play_continue = False
	else:
		draw_square(sqx1, sqy1, clr)
		draw_square(sqx2, sqy2, clr)
		draw_square(sqx3, sqy3, clr)
		draw_square(sqx4, sqy4, clr)

# DRAW NEXT SHAPE **************************************************************
# Draws shape in the next shape area

def draw_next_shape(p):
	# Default Start Location
	
	sq1x = 0
	sq2x = 0
	sq3x = 0
	sq4x = 0
	sq1y = 0
	sq2y = 0
	sq3y = 0
	sq4y = 0
	
	x = 310
	y = 40
	c = (0, 0, 0)
	
	draw_square(x-(size/2), y-(size/2), c)
	draw_square(x-(size/2), y+(size/2), c)
	draw_square(x-size, y-(size/2), c)
	draw_square(x-size, y+(size/2), c)
	draw_square(x-(size/2 + size), y-(size/2), c)
	draw_square(x-(size/2 + size), y+(size/2), c)
	
	draw_square(x+(size/2), y-(size/2), c)
	draw_square(x+(size/2), y+(size/2), c)
	draw_square(x+size, y-(size/2), c)
	draw_square(x+size, y+(size/2), c)
	draw_square(x+(size/2 + size), y-(size/2), c)
	draw_square(x+(size/2 + size), y+(size/2), c)
	
	c = (255, 255, 255)
	
	if (p == 1):
		# SQUARE
		sq1x = x - (size/2)
		sq1y = y - (size/2)
		sq2x = x + (size/2)
		sq2y = y - (size/2)
		sq3x = x + (size/2)
		sq3y = y + (size/2)
		sq4x = x - (size/2)
		sq4y = y + (size/2)
		
		c = (255, 255, 0)
		
	elif (p == 2):
		# LINE
		
		sq1x = x - (size/2 + size)
		sq2x = x - (size/2)
		sq3x = x + (size/2)
		sq4x = x + (size/2 + size)
		sq1y = y - (size/2)
		sq2y = y - (size/2)
		sq3y = y - (size/2)
		sq4y = y - (size/2)
		
		c = (0, 255, 255)
	
	elif (p == 3):
		# S-SHAPE
	
		sq1x = x + (size/2)
		sq2x = x + (size/2 + size)
		sq3x = x + (size/2)
		sq4x = x - (size/2)
		sq1y = y - (size/2)
		sq2y = y - (size/2)
		sq3y = y + (size/2)
		sq4y = y + (size/2)
		
		c = (0, 255, 0)
	
	elif (p == 4):
		# Z-SHAPE
		
		sq1x = x - (size/2)
		sq2x = x - (size/2 + size)
		sq3x = x - (size/2)
		sq4x = x + (size/2)
		sq1y = y - (size/2)
		sq2y = y - (size/2)
		sq3y = y + (size/2)
		sq4y = y + (size/2)
		
		c = (255, 0, 0)
		
	elif (p == 5):
		# T-SHAPE
		
		sq1x = x + (size/2)
		sq2x = x + (size/2)
		sq3x = x + (size/2 + size)
		sq4x = x - (size/2)
		sq1y = y - (size/2)
		sq2y = y + (size/2)
		sq3y = y + (size/2)
		sq4y = y + (size/2)
		
		c = (255, 0, 255)
	
	elif (p == 6):
		# L-SHAPE
		
		sq1x = x + (size/2)
		sq2x = x + (size/2)
		sq3x = x - (size/2)
		sq4x = x - (size/2 + size)
		sq1y = y - (size/2)
		sq2y = y + (size/2)
		sq3y = y + (size/2)
		sq4y = y + (size/2)
		
		c = (255, 128, 0)
		
	elif (p == 7):
		# MIRRORED L-SHAPE
		
		sq1x = x - (size/2)
		sq2x = x - (size/2)
		sq3x = x + (size/2)
		sq4x = x + (size/2 + size)
		sq1y = y - (size/2)
		sq2y = y + (size/2)
		sq3y = y + (size/2)
		sq4y = y + (size/2)
		
		c = (0, 0, 204)
		
	draw_square(sq1x, sq1y, c)
	draw_square(sq2x, sq2y, c)
	draw_square(sq3x, sq3y, c)
	draw_square(sq4x, sq4y, c)

# DRAW SQUARES *****************************************************************
# Draws square based on the location of the center of the square

def draw_square(x, y, c):
	pygame.draw.rect(screen, c, pygame.Rect(x-(size/2)*(4/5), y-(size/2)*(4/5), size*(4/5), size*(4/5)))

# CHECK LINES ******************************************************************
# Checks lines to see if any are full (should then be cleared)

def check_lines(y):
	global score
	global level

	line_deleted = 0
	locked_color = (255, 255, 255)
	for z in range (y, 30, -1*size):
		full = 0
		
		for x in range (30, 230, size):
			if (screen.get_at((x, z))[:3] == locked_color):
				full = full + 1
		
		if (full == 10):
			delete_line(z)
			line_deleted = line_deleted + 1
	
	score = score + line_deleted*100*level*line_deleted
	
# DELETE LINE ******************************************************************
# Delete line at given y-coordinate

def delete_line(y):
	for z in range(y, 50, -1*size):
		for x in range(30, 230, size):
			draw_square(x, z, screen.get_at((x, z-size))[:3])
	for x in range(30, 210, size):
		draw_square(x, 30, (0, 0, 0))
	
	check_lines(y)

# ROTATE ***********************************************************************
# Rotates shape clockwise

def rotate():
	global sqx1
	global sqx2
	global sqx3
	global sqx4
	global sqy1
	global sqy2
	global sqy3
	global sqy4
	global clr
	global centerx
	global centery
	
	draw_square(sqx1, sqy1, (0, 0, 0))
	draw_square(sqx2, sqy2, (0, 0, 0))
	draw_square(sqx3, sqy3, (0, 0, 0))
	draw_square(sqx4, sqy4, (0, 0, 0))

	x = sqx1 - centerx
	y = sqy1 - centery	
	newx1 = centerx - y
	newy1 = centery + x
	
	x = sqx2 - centerx
	y = sqy2 - centery
	newx2 = centerx - y
	newy2 = centery + x
	
	x = sqx3 - centerx
	y = sqy3 - centery
	newx3 = centerx - y
	newy3 = centery + x
	
	x = sqx4 - centerx
	y = sqy4 - centery
	newx4 = centerx - y
	newy4 = centery + x
	
	locked_color = (255, 255, 255)
	
	if (newx1 >= R_EDGE) or (newx2 >= R_EDGE) or (newx3 >= R_EDGE) or (newx4 >= R_EDGE):
		# would collide with right wall
		pass
	elif (newx1 <= L_EDGE) or (newx2 <= L_EDGE) or (newx3 <= L_EDGE) or (newx4 <= L_EDGE):
		# would collide with left wall
		pass
	elif (newy1 >= BOTTOM) or (newy2 >= BOTTOM) or (newy3 >= BOTTOM) or (newy4 >= BOTTOM):
		# would collide with bottom
		pass
	elif (newy1 <= TOP) or (newy2 <= TOP) or (newy3 <= TOP) or (newy4 <= TOP):
		# would collide with bottom
		pass
	elif (screen.get_at((int(newx1), int(newy1)))[:3] == locked_color):
		# would collide with other piece
		pass             
	elif (screen.get_at((int(newx2), int(newy2)))[:3] == locked_color):
		# would collide with other piece
		pass             
	elif (screen.get_at((int(newx3), int(newy3)))[:3] == locked_color):
		# would collide with other piece
		pass
	elif (screen.get_at((int(newx4), int(newy4)))[:3] == locked_color):
		# would collide with other piece
		pass
	else:
		sqx1 = newx1
		sqx2 = newx2
		sqx3 = newx3
		sqx4 = newx4
		
		sqy1 = newy1
		sqy2 = newy2
		sqy3 = newy3
		sqy4 = newy4
	
	draw_square(sqx1, sqy1, clr)
	draw_square(sqx2, sqy2, clr)
	draw_square(sqx3, sqy3, clr)
	draw_square(sqx4, sqy4, clr)
	
# DOWN *************************************************************************
# Shifts piece down of locked piece in place if nowhere to go

def down():
	locked_color = (255, 255, 255)
	global sqx1
	global sqx2
	global sqx3
	global sqx4
	global sqy1
	global sqy2
	global sqy3
	global sqy4
	global clr
	global centerx
	global centery
	global unlocked
	global score
	global level
	
	oldscore = 0
	
	if (screen.get_at((int(sqx1), int(sqy1+size)))[:3] == locked_color) or (screen.get_at((int(sqx2), int(sqy2+size)))[:3] == locked_color) or (screen.get_at((int(sqx3), int(sqy3+size)))[:3] == locked_color) or (screen.get_at((int(sqx4), int(sqy4+size)))[:3] == locked_color):
		# object collided with piece below (locks in place)
		
		# overwrite original squares white (locked)
		draw_square(sqx1, sqy1, (255, 255, 255))
		draw_square(sqx2, sqy2, (255, 255, 255))
		draw_square(sqx3, sqy3, (255, 255, 255))
		draw_square(sqx4, sqy4, (255, 255, 255))
		
		# increment score for placing piece
		oldscore = score
		score = score + 1*level
		
		# checks lines to see if clear necessary
		unlocked = False
		check_lines(BOTTOM-10)
		return
	
	elif ((sqy1+size >= BOTTOM) or (sqy2+size >= BOTTOM) or (sqy3+size >= BOTTOM) or (sqy4+size >= BOTTOM)):
		# object hit bottom of the board (locks in place)
		
		# overwrite original squares white (locked)
		draw_square(sqx1, sqy1, (255, 255, 255))
		draw_square(sqx2, sqy2, (255, 255, 255))
		draw_square(sqx3, sqy3, (255, 255, 255))
		draw_square(sqx4, sqy4, (255, 255, 255))
		
		# increment score for placing piece
		oldscore = score
		score = score + 1*level
		
		# checks lines to see if clear necessary
		unlocked = False
		check_lines(BOTTOM-10)
		return
		
	else:
		# overwrite original squares black
		draw_square(sqx1, sqy1, (0, 0, 0))
		draw_square(sqx2, sqy2, (0, 0, 0))
		draw_square(sqx3, sqy3, (0, 0, 0))
		draw_square(sqx4, sqy4, (0, 0, 0))
		
		# update values to shifted values
		sqy1 = sqy1 + size
		sqy2 = sqy2 + size
		sqy3 = sqy3 + size
		sqy4 = sqy4 + size
		centery = centery + size
		
		# draw new squares
		draw_square(sqx1, sqy1, clr)
		draw_square(sqx2, sqy2, clr)
		draw_square(sqx3, sqy3, clr)
		draw_square(sqx4, sqy4, clr)
	
# RSHIFT ************************************************************************
# Shifts piece right if space
def rshift():
	locked_color = (255, 255, 255)
	global sqx1
	global sqx2
	global sqx3
	global sqx4
	global sqy1
	global sqy2
	global sqy3
	global sqy4
	global clr
	global centerx
	global centery
	
	if (screen.get_at((int(sqx1+size), int(sqy1)))[:3] == locked_color) or (screen.get_at((int(sqx2+size), int(sqy2)))[:3] == locked_color) or (screen.get_at((int(sqx3+size), int(sqy3)))[:3] == locked_color) or (screen.get_at((int(sqx4+size), int(sqy4)))[:3] == locked_color):
		# object in the way, unable to shift
		return
		
	elif ((sqx1 + size) > R_EDGE or (sqx2 + size) > R_EDGE or (sqx3 + size) > R_EDGE or (sqx4 + size) > R_EDGE):
		# edge condition, unable to shift
		return
		
	else:
		# overwrite original squares black
		draw_square(sqx1, sqy1, (0, 0, 0))
		draw_square(sqx2, sqy2, (0, 0, 0))
		draw_square(sqx3, sqy3, (0, 0, 0))
		draw_square(sqx4, sqy4, (0, 0, 0))
		
		# update values to shifted values
		sqx1 = sqx1 + size
		sqx2 = sqx2 + size
		sqx3 = sqx3 + size
		sqx4 = sqx4 + size
		centerx = centerx + size
		
		# draw new squares
		draw_square(sqx1, sqy1, clr)
		draw_square(sqx2, sqy2, clr)
		draw_square(sqx3, sqy3, clr)
		draw_square(sqx4, sqy4, clr)
	
# LSHIFT ***********************************************************************
# Shifts piece left if space

def lshift():
	locked_color = (255, 255, 255)
	global sqx1
	global sqx2
	global sqx3
	global sqx4
	global sqy1
	global sqy2
	global sqy3
	global sqy4
	global clr
	global centerx
	global centery
	
	if (screen.get_at((int(sqx1-size), int(sqy1)))[:3] == locked_color) or (screen.get_at((int(sqx2-size), int(sqy2)))[:3] == locked_color) or (screen.get_at((int(sqx3-size), int(sqy3)))[:3] == locked_color) or (screen.get_at((int(sqx4-size), int(sqy4)))[:3] == locked_color):
		# object in the way, unable to shift
		return
	
	elif ((sqx1 - size) < L_EDGE or (sqx2 - size) < L_EDGE or (sqx3 - size) < L_EDGE or (sqx4 - size) < L_EDGE):
		# edge condition, unable to shift
		return
	
	else:
		# overwrite original squares black
		draw_square(sqx1, sqy1, (0, 0, 0))
		draw_square(sqx2, sqy2, (0, 0, 0))
		draw_square(sqx3, sqy3, (0, 0, 0))
		draw_square(sqx4, sqy4, (0, 0, 0))
		
		# update values to shifted values
		sqx1 = sqx1 - size
		sqx2 = sqx2 - size
		sqx3 = sqx3 - size
		sqx4 = sqx4 - size
		centerx = centerx - size
		
		# draw new squares
		draw_square(sqx1, sqy1, clr)
		draw_square(sqx2, sqy2, clr)
		draw_square(sqx3, sqy3, clr)
		draw_square(sqx4, sqy4, clr)
		
# NEW PIECE ********************************************************************
# Random generation of next piece (returns random number between 1 and 7

def new_piece():
	return random.randint(1,7)